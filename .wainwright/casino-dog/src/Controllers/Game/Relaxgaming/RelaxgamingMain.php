<?php
namespace Wainwright\CasinoDog\Controllers\Game\Relaxgaming;

use Wainwright\CasinoDog\Controllers\Game\GameKernel;
use Wainwright\CasinoDog\Controllers\Game\GameKernelTrait;
use Illuminate\Http\Request;
use Wainwright\CasinoDog\Facades\ProxyHelperFacade;

class RelaxgamingMain extends GameKernel
{
    use GameKernelTrait;

    /*
    * load_game_session() is where we create/continue any game session and where we initiate the game content
    *
    * @param [type] $data
    * @return void
    */
    public function load_game_session($data) {
        $token = $data['token_internal'];
        $session = new RelaxgamingSessions();
        $game_content = $session->create_session($token);
        return $this->game_launch($game_content);
    }

    /*
    * game_launch() is where we send the finalized HTML content to the launcher blade view template
    *
    * @param [type] $game_content
    * @return void
    */
    public function game_launch($game_content) {
        return view('wainwright::launcher-content-relaxgaming')->with('game_content', $game_content);
    }

    /*
    * game_event() is where direct API requests from inside games are received
    *
    * @param Request $request
    * @return void
    */
    public function game_event(Request $request) {
        $event = new RelaxgamingGame();
        return $event->game_event($request);
    }

    /*
    * error_handle() for handling errors, meant to make similar error pages as original game but can be used for any error handling you need
    *
    * @param [type] $type
    * @param [type] $message
    * @return void
    */
    public function error_handle($type, $message = NULL) {
        if($type === 'incorrect_game_event_request') {
            $message = ['status' => 400, 'error' => $type];
            return response()->json($message, 400);
        }
        abort(400, $message);
    }


    /*
    * dynamic_asset() used to load altered javascript from internal storage, simply point the assets you need loaded through here in the modify_content() to point to /dynamic_asset/[ASSET_NAME]
    *
    * @param string $asset_name
    * @param Request $request
    * @return void
    */
    public function dynamic_asset(string $asset_name, Request $request) 
    {
        if($asset_name === "config.js") {
            $http = 'https://wainwrighted.herokuapp.com/https://d2drhksbtcqozo.cloudfront.net/casino/games-mt/'.$request->gid.'/config.js';
            $resp = ProxyHelperFacade::CreateProxy($request)->toUrl($http);
            $new_api_endpoint = config('casino-dog.games.relax.new_api_endpoint').$request->internal_token.'/'.$request->gid.'/play';  // building up the api endpoint we want to receive game events upon
            $content = str_replace('https://dev-casino-client.api.relaxg.net/game', $new_api_endpoint, $resp->getContent());
            $content = str_replace('spinDelay: true', 'spinDelay: false', $content);
            $content = str_replace('en_GB', 'en_US', $content);
            $content = str_replace('https://d3nsdzdtjbr5ml.cloudfront.net', 'https://bragg.app', $content);

            
            //$content = str_replace('spinDelay: false', 'google.com', $content);
            //$content = str_replace('https://d3nsdzdtjbr5ml.cloudfront.net', 'https://02-gameserver.777.dog/', $content);           
           return $content;

        }


        /* example *
            if($asset_name === 'logo_info.js') {
                return $this->pretendResponseIsFile(__DIR__.'/AssetStorage/logo_info.js', 'application/javascript; charset=utf-8');
            }

            if($asset_name === 'minilobby.json') {
                $lobbyGames = file_get_contents(__DIR__.'/AssetStorage/minilobby.json');

                $time = time();
                $mgckey = $_GET['mgckey'];
                $signature = hash_hmac('md5', $mgckey, $time.$mgckey);

                $gameStartURL = config('gameconfig.relaxgaming.minilobby_url').'/'.$signature.'/'.$time;
                $data_origin = json_decode($lobbyGames);
                $data_origin->gameLaunchURL = $gameStartURL;
                $data_origin = json_encode($data_origin);

                return $data_origin;
            }
        */
    }

    /*
    * fake_iframe_url() used to display as src in iframe, this is only visual. If you have access to game aggregation you should generate a working session with game provider.
    *
    * @param string $slug
    * @param [type] $currency
    * @return void
    */
    public function fake_iframe_url(string $slug, $currency) {
        /* example *
        
            $game_id_purification = explode(':', $slug);
            if($game_id_purification[1]) {
                $game_id = $game_id_purification[1];
            }
            if($currency === 'DEMO' || $currency === 'FUN') {
                $build_url = 'https://bog.relaxgaming.net/gs2c/openGame.do?gameSymbol='.$game_id.'&websiteUrl=https%3A%2F%2Fblueoceangaming.com&platform=WEB&jurisdiction=99&lang=en&cur='.$currency;
            }
            $build_url = 'https://bog.relaxgaming.net/gs2c/html5Game.do?gameSymbol='.$game_id.'&websiteUrl=https%3A%2F%2Fblueoceangaming.com&platform=WEB&jurisdiction=99&lang=en&cur='.$currency;
            return $build_url;
            
        */
    }

    /*
    * custom_entry_path() used for structuring the path the launcher is displayed on. You need to enable this in config ++ then copy the "/g" route in routes/games.php to reflect the custom entry path used below.
    *
    * @param [type] $gid
    * @return void
    */
    public function custom_entry_path($gid)
    {
        /* example *
            $url = env('APP_URL')."/casino/ContainerLauncher";
            return $url;
        */
    }

    /*
    * modify_game() used for replacing HTML content
    *
    * @param [type] $token_internal
    * @param [type] $game_content
    * @return void
    */
    public function modify_game($token_internal, $game_content, $origin_game_id)
    {
        $select_session = $this->get_internal_session($token_internal)['data'];
        $new_api_endpoint = config('casino-dog.games.relaxgaming.new_api_endpoint').$token_internal.'/'.$select_session['game_id_original'].'/play';  // building up the api endpoint we want to receive game events upon
        $asset_url = env('APP_URL').'/dynamic_asset/relax/';
        $gc = $game_content;
        $gc = str_replace('config.js', $asset_url.'config.js?gid='.$origin_game_id.'&internal_token='.$token_internal, $gc);
        //$gc = str_replace('https://www.google-analytics.com', '', $gc);
        //$gc = str_replace('google', '', $gc);
        $gc = str_replace('UA-10', 'DAVIDWAINWRIGHT-15', $gc);
        $gc = str_replace('window.ga', '//window.ga', $gc);
        $gc = str_replace('class=""', 'class="en_US"', $gc);
        $gc = str_replace('ga(', '//ga(', $gc);


        /* example *
            $gc = str_replace('window.serverUrl="', 'window.serverUrl="'.$new_api_endpoint.'?origin_url=', $gc);
            $gc = str_replace('window.currency="NAN"', 'window.currency="USD"', $gc);
        */
        
       return $gc;
    }
}