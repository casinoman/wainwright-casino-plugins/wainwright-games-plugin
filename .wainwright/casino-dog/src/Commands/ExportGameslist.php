<?php

namespace Wainwright\CasinoDog\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use RuntimeException;
use Symfony\Component\Process\Process;
use DB;
use Wainwright\CasinoDog\Models\OperatorAccess;
class ExportGameslist extends Command
{

    protected $signature = 'casino-dog:export-gameslist';

    public $description = 'Save gameslist to .json per provider';

    public function handle()
    {   
        $gameprovider = $this->ask('Enter gameprovider name you wish to export');
        
        $count = DB::table('wainwrighted_gameslist')->where('provider', $gameprovider)->count();
        if($count < 1) {
            $this->error('No games found for '.$gameprovider);
        }

        $gameslist = DB::table('wainwrighted_gameslist')->where('provider', $gameprovider)->all();

        return self::SUCCESS;
    }
}
